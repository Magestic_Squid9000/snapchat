package com.example.student.snapchatproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID = "AFB98A2A-61F2-2CA3-FF61-74C1C140AA00";
    public static final String SECRET_KEY = "15E2BFA7-FD7F-EBA9-FF54-B4751E31A300";
    public static final String VERSION= "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainMenuFragment mainMenu = new MainMenuFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();

        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
    }
}
